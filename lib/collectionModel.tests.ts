import { rlFakeAsync, mock, IMockedRequest } from 'rl-async-testing';

import { CollectionModel } from './collectionModel';

interface ITestObject {
	id: number;
	prop?: number;
}

interface IHttpMock {
	get?: IMockedRequest<ITestObject[]>;
	post?: IMockedRequest<ITestObject>;
	put?: IMockedRequest<ITestObject>;
	delete?: IMockedRequest<void>;
}

describe('CollectionModel', () => {
	let model: CollectionModel<ITestObject>;
	let http: IHttpMock;

	beforeEach(() => {
		http = {
			get: mock.request(),
			post: mock.request(),
			put: mock.request(),
			delete: mock.request(),
		};
		model = new CollectionModel<ITestObject>('/test', <any>http);

		expect(model.url).to.equal('/test');
	});

	it('should make an http request to get the list, then push it to the stream', rlFakeAsync(() => {
		let list;
		const listFromServer = [{ id: 1 }, { id: 2 }, { id: 3 }];
		http.get = mock.request(listFromServer);

		model.subscribe(data => list = data);

		expect(list).to.be.null;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(list).to.deep.equal(listFromServer);
	}));

	it('should return the http request', rlFakeAsync(() => {
		const listFromServer = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let listReturnedFromAction;
		const completeSpy = sinon.spy();
		http.get = mock.request(listFromServer);
		(model as any).init();

		model.load().subscribe(data => listReturnedFromAction = data, null, completeSpy);

		expect(listReturnedFromAction).to.not.exist;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(listReturnedFromAction).to.deep.equal(listFromServer);
		sinon.assert.calledOnce(completeSpy);
	}));

	it('should make an http request to create a new item, then push the updated list to the stream', rlFakeAsync(() => {
		let list = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let newItem = { id: 4 };
		let listWithNewItem = [...list, newItem];
		let listFromDataStream;
		let itemFromServer;
		http.post = mock.request((url, model) => model);
		loadList(list);

		model.subscribe(data => listFromDataStream = data);
		model.add(newItem).subscribe(data => itemFromServer = data);

		expect(listFromDataStream).to.deep.equal(list);
		expect(itemFromServer).to.not.exist;
		sinon.assert.calledOnce(http.post);

		http.get.flush();
		http.post.flush();

		expect(listFromDataStream).to.deep.equal(listWithNewItem);
		expect(itemFromServer).to.equal(newItem);
	}));

	it('should make an http request to update an item, then push the updated list to the stream', rlFakeAsync(() => {
		let list = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let updatedItem = { id: 2, prop: 5 };
		let listWithUpdatedItem = [list[0], updatedItem, list[2]];
		let listFromDataStream;
		let itemFromServer;
		http.put = mock.request((url, model) => model);
		loadList(list);

		model.subscribe(data => listFromDataStream = data);
		model.update(updatedItem, list[1]).subscribe(data => itemFromServer = data);

		expect(listFromDataStream).to.deep.equal(list);
		expect(itemFromServer).to.not.exist;
		sinon.assert.calledOnce(http.put);

		http.get.flush();
		http.put.flush();

		expect(listFromDataStream).to.deep.equal(listWithUpdatedItem);
		expect(itemFromServer).to.equal(updatedItem);
	}));

	it('should make an http request to delete an item, then push the updated list to the stream', rlFakeAsync(() => {
		let list = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let deleteItem = list[1];
		let listWithMissingItem = [list[0], list[2]];
		let listFromDataStream;
		loadList(list);

		model.subscribe(data => listFromDataStream = data);
		model.remove(deleteItem).subscribe();

		expect(listFromDataStream).to.deep.equal(list);
		sinon.assert.calledOnce(http.delete);

		http.get.flush();
		http.delete.flush();

		expect(listFromDataStream).to.deep.equal(listWithMissingItem);
	}));

	it('should map the list using the specified transform with a load action', rlFakeAsync(() => {
		let list;
		const listFromServer = [{ id: 1 }, { id: 2 }, { id: 3 }];
		const expectedListResult = [{ id: 2 }, { id: 4 }, { id: 6 }];
		http.get = mock.request(listFromServer);
		model.transform = (item: ITestObject) => {
			item.id = item.id * 2;
			return item;
		};

		model.subscribe(data => list = data);

		expect(list).to.be.null;
		sinon.assert.calledOnce(http.get);

		http.get.flush();

		expect(list).to.deep.equal(expectedListResult);
	}));

	it('should map the item using the specified transform with a create action', rlFakeAsync(() => {
		let list = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let newItem = { id: 4 };
		let itemFromServer;
		let expectedResult = { id: 8 };
		http.post = mock.request((url, model) => model);
		loadList(list);
		model.transform = (item: ITestObject) => {
			item.id = item.id * 2;
			return item;
		};

		model.add(newItem).subscribe(data => itemFromServer = data);

		http.get.flush();
		http.post.flush();

		expect(itemFromServer).to.deep.equal(expectedResult);
	}));

	it('should map the item using the specified transform with an update action', rlFakeAsync(() => {
		let list = [{ id: 1 }, { id: 2 }, { id: 3 }];
		let updatedItem = { id: 2, prop: 5 };
		let listFromDataStream;
		let itemFromServer;
		let expectedResult = { id: 2, prop: 10 };
		http.put = mock.request((url, model) => model);
		loadList(list);
		model.transform = (item: ITestObject) => {
			item.prop = item.prop * 2;
			return item;
		};

		model.update(updatedItem, list[1]).subscribe(data => itemFromServer = data);

		http.get.flush();
		http.put.flush();

		expect(itemFromServer).to.deep.equal(expectedResult);
	}));

	function loadList(list: ITestObject[]): void {
		http.get = mock.request(list);
		model.subscribe();
		http.get.flush();
		http.get.reset();
	}
});
