var map = {
	'@angular': 'node_modules/@angular',
	'lodash': 'node_modules/lodash/index',
	'rl-async-testing': 'node_modules/rl-async-testing',
	'rl-http': 'node_modules/rl-http',
	'rxjs': 'node_modules/rxjs',
};

var angularPackageNames = [
	'core',
	'compiler',
	'common',
	'platform-browser',
	'platform-browser-dynamic',
	'http',
];

var defaultPackages = [
	'rl-async-testing',
	'rl-http',
];

var packages = {
	'lib': {
		defaultExtension: 'js',
	},
	'node_modules': {
		defaultExtension: 'js',
	},
	'rxjs': {
		main: 'Rx.js',
	},
};

function setAngularPackage(packageName) {
	map[`@angular/${packageName}`] = `node_modules/@angular/${packageName}/bundles/${packageName}.umd.js`;
}

function setAngularTestingPackage(packageName) {
	map[`@angular/${packageName}/testing`] = `node_modules/@angular/${packageName}/bundles/${packageName}-testing.umd.js`;
}

function setDefaultPackage(packageName) {
	packages[packageName] = { main: 'index.js' };
}

angularPackageNames.forEach(setAngularPackage);
angularPackageNames.forEach(setAngularTestingPackage);
defaultPackages.forEach(setDefaultPackage);

System.config({
	map: map,
	packages: packages,
});
